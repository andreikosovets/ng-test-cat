class DropdownlistController {
  constructor($scope) {
    this.name = 'dropdownlist';
    this.methods($scope)
  }

  methods($scope){
    $scope.selectSort = (value)=>{
      this.sorting.currentSort = value;
      $scope.openSort = false;
      this.onSelectSort({
        $event: {value: value}
      })
    }
  }
}

DropdownlistController.$inject = ['$scope'];
export default DropdownlistController;
