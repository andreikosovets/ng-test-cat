import template from './dropdownlist.html';
import controller from './dropdownlist.controller';
import './dropdownlist.scss';

let dropdownlistComponent = {
  bindings: {
    sorting: '<',
    onSelectSort: '&'
  },
  template,
  controller
};

export default dropdownlistComponent;
