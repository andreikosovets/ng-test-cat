import angular from 'angular';
import uiRouter from 'angular-ui-router';
import dropdownlistComponent from './dropdownlist.component';

let dropdownlistModule = angular.module('dropdownlist', [
  uiRouter
])

.component('dropdownlist', dropdownlistComponent)

.name;

export default dropdownlistModule;
