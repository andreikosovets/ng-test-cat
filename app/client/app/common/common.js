import angular from 'angular';
import Dropdown from './dropdownlist/dropdownlist';

let commonModule = angular.module('app.common', [
  Dropdown
])

.name;

export default commonModule;
