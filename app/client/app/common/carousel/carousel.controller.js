class CarouselController {
  constructor() {
    this.name = 'carousel';
  }

  moveSlide(e,direction){
    let elem = angular.element(e.currentTarget);
    let sliderScope = elem.parent().next().scope();

    switch(direction){
      case "next" :
        sliderScope.nextSlide();
        break;
      case "prev" :
        sliderScope.prevSlide();
    }
  }

}

CarouselController.$inject = ['$scope'];

export default CarouselController;
