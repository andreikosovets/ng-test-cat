import template from './filterComponent.html';
import controller from './filterComponent.controller';
import './filterComponent.scss';

let filterComponentComponent = {
  bindings: {},
  template,
  controller
};

export default filterComponentComponent;
