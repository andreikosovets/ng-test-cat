import angular from 'angular';
import uiRouter from 'angular-ui-router';
import filterComponentComponent from './filterComponent.component';
import dropdownLinkComponent from '../dropdownLink/dropdownLink.component'

let filterComponentModule = angular.module('filterComponent', [
  uiRouter
])

.component('filterComponent', filterComponentComponent)

.name;

export default filterComponentModule;
