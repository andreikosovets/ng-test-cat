import angular from 'angular';
import 'angular-animate';
import angularTouch from 'angular-touch';
import uiRouter from 'angular-ui-router';
import Common from './common/common';
import Components from './components/components';
import AppComponent from './app.component';
import 'normalize.css';
import "angular-carousel";
import "angular-carousel/dist/angular-carousel.min.css";
import remoteData from "./services/getData"

angular.module('app', [
    uiRouter,
    Common,
    Components,
    angularTouch,
    "angular-carousel",
    'ngAnimate'
  ])
  .config(($locationProvider) => {
    "ngInject";
    // @see: https://github.com/angular-ui/ui-router/wiki/Frequently-Asked-Questions
    // #how-to-configure-your-server-to-work-with-html5mode
    $locationProvider.html5Mode(true).hashPrefix('!');
  })
  .service('remoteData', ['$http',(($http) => new remoteData($http))])

  .component('app', AppComponent);
