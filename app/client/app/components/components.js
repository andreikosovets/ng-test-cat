import angular from 'angular';
import Home from './home/home';
import Detail from './detailPage/detailPage';
import Header from '../common/header/header'

let componentModule = angular.module('app.components', [
  Home,
  Detail,
  Header
])

.name;

export default componentModule;
