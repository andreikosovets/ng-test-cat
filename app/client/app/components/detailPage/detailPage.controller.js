class DetailPageController {
  constructor($scope,$stateParams,remoteData) {
    this.name = 'detailPage';
    this.remoteData = remoteData;
    this.init($stateParams.productId, $scope);
  }

  init(productId,$scope){
    this.remoteData.getById(productId).then((data)=>{
      $scope.product = data;
      $scope.$apply();
    })
  }
}

DetailPageController.$inject = ['$scope','$stateParams','remoteData'];

export default DetailPageController;
