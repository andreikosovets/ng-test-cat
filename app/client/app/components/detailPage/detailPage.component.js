import template from './detailPage.html';
import controller from './detailPage.controller';
import './detailPage.scss';

let detailPageComponent = {
  bindings: {},
  template,
  controller
};

export default detailPageComponent;
