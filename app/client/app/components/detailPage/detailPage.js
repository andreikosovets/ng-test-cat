import angular from 'angular';
import uiRouter from 'angular-ui-router';
import detailPageComponent from './detailPage.component';

let detailPageModule = angular.module('detailPage', [
  uiRouter
])

.config(($stateProvider) => {
  "ngInject";
  $stateProvider
    .state('detail', {
      url: '/detail/:productId',
      component: 'detailPage'
    });
})

.component('detailPage', detailPageComponent)

.name;

export default detailPageModule;
