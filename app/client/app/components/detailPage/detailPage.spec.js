import DetailPageModule from './detailPage';
import DetailPageController from './detailPage.controller';
import DetailPageComponent from './detailPage.component';
import DetailPageTemplate from './detailPage.html';

describe('DetailPage', () => {
  let $rootScope, makeController;

  beforeEach(window.module(DetailPageModule));
  beforeEach(inject((_$rootScope_) => {
    $rootScope = _$rootScope_;
    makeController = () => {
      return new DetailPageController();
    };
  }));

  describe('Module', () => {
    // top-level specs: i.e., routes, injection, naming
  });

  describe('Controller', () => {
    // controller specs
    it('has a name property [REMOVE]', () => { // erase if removing this.name from the controller
      let controller = makeController();
      expect(controller).to.have.property('name');
    });
  });

  describe('Template', () => {
    // template specs
    // tip: use regex to ensure correct bindings are used e.g., {{  }}
    it('has name in template [REMOVE]', () => {
      expect(DetailPageTemplate).to.match(/{{\s?\$ctrl\.name\s?}}/g);
    });
  });

  describe('Component', () => {
    // component/directive specs
    let component = DetailPageComponent;

    it('includes the intended template', () => {
      expect(component.template).to.equal(DetailPageTemplate);
    });

    it('invokes the right controller', () => {
      expect(component.controller).to.equal(DetailPageController);
    });
  });
});
