import angular from 'angular';
import uiRouter from 'angular-ui-router';
import homeComponent from './home.component';
import filterComponent from '../../common/filterComponent/filterComponent.component';
import carouselComponent from '../../common/carousel/carousel.component';

let homeModule = angular.module('home', [
  uiRouter
])

.config(($stateProvider, $urlRouterProvider) => {
  "ngInject";

  $urlRouterProvider.otherwise('/');

  $stateProvider
    .state('home', {
      url: '/',
      component: 'home'
    });
})

.component('home', homeComponent)
.component('filter', filterComponent)
.component('carousel', carouselComponent)

.name;

export default homeModule;
