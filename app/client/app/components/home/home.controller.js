class HomeController {
  constructor($scope,remoteData,$window) {
    this.name = 'home';
    this.init($scope,remoteData,$window);
    this.watchers($scope,$window);
  }

  init($scope,remoteData,$window){

    $scope.filter = {
      man:true,
      woman:true,
      children:false,
    };

    remoteData.getAll().then((data)=>{
      this.data = data;
      this.prepareDataForSlider(this.data, $scope);
      this.sorting = {
        currentSort: "no select",
        sortValues : [
          "no select",
          "price",
          "name"
        ]
      };

      $scope.$apply();
      $window.dispatchEvent(new Event("resize"));
    });

    this.watchMobileForRebaseCarousel($scope,$window);


  }

  watchMobileForRebaseCarousel($scope,$window){
    let isMobile = $window.innerWidth <= 767;
    let that = this;
    angular.element($window).bind('resize', function(){
      if (isMobile && $window.innerWidth <= 767){
        that.prepareDataForSlider(that.data,$scope,null,1);
        isMobile = false;
      }
      if(!isMobile && $window.innerWidth > 767){
        that.prepareDataForSlider(that.data,$scope);
        isMobile = true;
      }
      $scope.$apply();
    });

  }

  prepareDataForSlider(data,$scope,sorting,slidesToShow = 0){
    if (sorting){
      data = this.sortProducts(data, sorting)
    }

    let returnArr = [];

    data.forEach((val,key,arr) => {
      let result = Object.assign({},val);
      let combinedPorducts = [];
      let tpmArr = [];
      if (result.products && result.products.length) {
        result.products.forEach((product,key1,arr1) => {
          tpmArr.push(product);
          /**
           * we combine for 3 product in slide, if we have last one< then push what we have
           */
          if ( (tpmArr.length === (slidesToShow || 3) ) || (arr1.length === (key1 +1)) ) {
            combinedPorducts.push(tpmArr);
            tpmArr = [];
          }
        });
      }
      result.products = combinedPorducts;
      result.fullCombinedLength = combinedPorducts.length;

      returnArr.push(result);
    });

    $scope.data = returnArr;
  }

  sortProducts(data, sorting){
    data.forEach((val,key,arr)=>{
      val.products.sort((a,b)=>{
        if (a[sorting] > b[sorting]) {
          return 1;
        }
        if (a[sorting] < b[sorting]) {
          return -1;
        }
        // a должно быть равным b
        return 0;
      })
    });

    return data;
  }

  watchers($scope,$window) {
    let ctrl = this;
    $scope.$watch("selectedSort", (newData, oldData, $scope)=>{
      if (newData && newData !== "no select") {
        if ($window.innerWidth <= 767){
          ctrl.prepareDataForSlider(ctrl.data,$scope,newData,1);
        }
        if($window.innerWidth > 767){
          ctrl.prepareDataForSlider(ctrl.data,$scope,newData);
        }

      }
    });
  }

}

HomeController.$inject = ['$scope','remoteData','$window'];
export default HomeController;
