class remoteData{

  constructor($http) {
    this.$http = $http;
  }

  getAll = ()=>{
    let that = this;
    return new Promise((res,rej)=>{
      that.$http.get("data/data.json")
        .then((response)=>{
          res(response.data);
        },(err)=>{
          rej(err);
        });
    })
  };

  getById = (id)=>{
    let that = this;
    if (!id) {
      return;
    }
    return new Promise((res,rej)=>{
      that.$http.get("data/data.json")
        .then((response)=>{
          let data = response.data;
          let result;
          data.forEach((val,key,arr)=>{
            val.products.forEach((val,key,arr)=>{
              if (val.id === parseInt(id)){
                result = val;
              }
            })
          });
          //debugger;
          res(result);
        },(err)=>{
          rej(err);
        });
    })
  };
}

export default remoteData
